#include "tst_recipebuilder.h"
#include "recipebuilder.h"
#include "recipe.h"

tst_recipebuilder::tst_recipebuilder()
{

}

void tst_recipebuilder::name(){
    QString name = "recipe name";
    RecipeBuilder builder = RecipeBuilder();

    builder.name(name);
    Recipe rep = builder.build();

    QCOMPARE(rep.name(), name);
}

void tst_recipebuilder::activeTime_data()
{
    QTest::addColumn<int>("time");

    QTest::newRow("positive") << 10;
    QTest::newRow("zero")     << 0;
    QTest::newRow("negative") << -10;
}

void tst_recipebuilder::activeTime(){
    QFETCH(int, time);
    RecipeBuilder builder = RecipeBuilder();

    try {
        builder.activeTime(time);
        Recipe rep = builder.build();
        QCOMPARE(rep.activeTime(), time);
    } catch (std::domain_error) {
        QCOMPARE(time < 0, true);
    }
}
