#ifndef TST_RECIPE_H
#define TST_RECIPE_H
#include <QtTest>

class tst_recipe : public QObject
{
    Q_OBJECT

public:
    tst_recipe();

private Q_SLOTS:
    void filename_data();
    void filename();
    //void deleteRecipeFile();
    void hasSource_data();
    void hasSource();
};
#endif // TST_RECIPE_H
