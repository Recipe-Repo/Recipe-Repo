#include "tst_recipe.h"
#include "recipebuilder.h"
#include "recipe.h"

tst_recipe::tst_recipe()
{

}

void tst_recipe::filename_data()
{
    QTest::addColumn<QString>("string");
    QTest::addColumn<QString>("result");

    QTest::newRow("all lower") << "hello" << "hello";
    QTest::newRow("mixed")     << "Hello" << "hello";
    QTest::newRow("all upper") << "HELLO" << "hello";
}

void tst_recipe::filename()
{
  QFETCH(QString, string);
  QFETCH(QString, result);
  Recipe testRep(string);

  QCOMPARE(testRep.filename(), result);
}

void tst_recipe::hasSource_data(){
  QTest::addColumn<QString>("source");
  QTest::addColumn<QString>("url");
  QTest::addColumn<bool>("result");

  QTest::newRow("source only") << "source" << QString() << true;
  QTest::newRow("url only")     << QString() << "url" << true;
  QTest::newRow("source and url") << "source" << "url" << true;
  QTest::newRow("none") << QString() << QString() << false;
}

void tst_recipe::hasSource(){
  QFETCH(QString, source);
  QFETCH(QString, url);
  QFETCH(bool, result);

  RecipeBuilder builder;
  builder.sourceName(source);
  builder.sourceURL(url);
  Recipe testRep = builder.build();

  QCOMPARE(testRep.hasSource(), result);
}
