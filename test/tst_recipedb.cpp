#include <QString>
#include <QDebug>

#include "tst_recipedb.h"
#include "recipedbmanager.h"
#include "recipe.h"

tst_recipeDB::tst_recipeDB()
{
}

void tst_recipeDB::importRecipe()
{
    RecipeDBManager testDBManager(new QObject(), ":memory:");
    Recipe testRep("Test Recipe");

    testDBManager.addRecipe(testRep);

    Recipe retrievedRep = testDBManager.retrieveRecipe("Test Recipe");
    QVERIFY2(retrievedRep.name() == testRep.name(), "Failure");
}
