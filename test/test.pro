#-------------------------------------------------
#
# Project created by QtCreator 2018-07-08T00:18:55
#
#-------------------------------------------------

QT       += core gui sql testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tst_recipedb
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/

CONFIG += c++11

unix:!macx: LIBS += -lboost_system
unix:!macx: LIBS += -lboost_filesystem

! include(../app/app.pri) {
    error( "Couldn't find the app.pri file!" )
} else {
    message("Loaded app.pri file into test.pro")
}

! include(../app/recipe_handling.pri) {
    error( "Couldn't find the recipe_handling.pri file!" )
} else {
    message("Loaded recipe_handling.pri file into app.pro")
}

SOURCES += tst_recipedb.cpp \
    main.cpp \
    tst_recipe.cpp \
    tst_recipebuilder.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    tst_recipedb.h \
    tst_recipe.h \
    tst_recipebuilder.h
