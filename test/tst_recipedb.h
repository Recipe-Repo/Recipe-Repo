#ifndef TST_RECIPEDB_H
#define TST_RECIPEDB_H
#include <QtTest>

class tst_recipeDB : public QObject
{
    Q_OBJECT

public:
    tst_recipeDB();

private Q_SLOTS:
    void importRecipe();
};

#endif // TST_RECIPEDB_H
