#include <QtTest>
#include <QApplication>
#include <QDebug>

#include "tst_recipebuilder.h"
#include <tst_recipe.h>
#include <tst_recipedb.h>

int main(int argc, char** argv)
{
   QApplication a(argc, argv);
   int status = 0;
   {
      tst_recipebuilder tc;
      status |= QTest::qExec(&tc, argc, argv);
   }
   {
      tst_recipe tc;
      status |= QTest::qExec(&tc, argc, argv);
   }
   {
      tst_recipeDB tc;
      status |= QTest::qExec(&tc, argc, argv);
   }

   return status;
}
