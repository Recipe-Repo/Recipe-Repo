#ifndef TST_RECIPEBUILDER_H
#define TST_RECIPEBUILDER_H
#include <QtTest>


class tst_recipebuilder : public QObject
{
    Q_OBJECT

public:
    tst_recipebuilder();

private Q_SLOTS:
    void name();
    void activeTime_data();
    void activeTime();
};

#endif // TST_RECIPEBUILDER_H
