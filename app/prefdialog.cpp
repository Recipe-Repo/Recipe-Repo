#include "prefdialog.h"
#include "ui_prefdialog.h"

#include <iostream>

#include <QDebug>
#include <QFileDialog>
#include <QSettings>

#include "settings.h"

QString pineappleImagePath;

PrefDialog::PrefDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PrefDialog)
{
    ui->setupUi(this);
    QSettings settings("Recipe Repo", "Recipe Repo");
    ui->Library_Path_lineEdit->setText(settings.value("Library location").toString());
}

PrefDialog::~PrefDialog()
{
    delete ui;
}

bool PrefDialog::loadSettings(){
  QSettings settings("Recipe Repo", "Recipe Repo");
//  if(!settings.value("Library location").toString().isEmpty()){
//    recipeLibraryPath = QDir(settings.value("Library location").toString());
//  }
//  else{
//   qDebug() <<"No library location found";
//   settings.setValue("Library location", "/home/tom/Recipes/");
//   recipeLibraryPath = "/home/tom/Recipes/";
//  }
  if(!settings.value("Pineapple").toString().isEmpty()){
    pineappleImagePath = settings.value("Pineapple").toString();
  }
  else{
    qDebug() << "No image file found";
    settings.setValue("Pineapple", "/home/tom/Documents/Programming Projects/Recipe-Repo/pineapple.png");
    pineappleImagePath = "/home/tom/Documents/Programming Projects/Recipe-Repo/pineapple.png";
  }
  bool success = true;
  return success;
}

bool PrefDialog::saveSettings(){
  QSettings settings("Recipe Repo", "Recipe Repo");
  settings.setValue("Library location", ui->Library_Path_lineEdit->text());
  //moveDirectoryRecursively(recipeLibraryPath, ui->Library_Path_lineEdit->text().toStdString());
  //recipeLibraryPath = QDir(settings.value("Library location").toString());
  return true;
}

void PrefDialog::on_buttonBox_accepted()
{
  saveSettings();
  this->accept();
}

void PrefDialog::on_Library_Path_pushButton_clicked()
{
    QString newLibraryPath = QFileDialog::getExistingDirectory(this, tr("Select a new folder")/*,recipeLibraryPath.path()*/);
    ui->Library_Path_lineEdit->setText(newLibraryPath);

}

void PrefDialog::on_buttonBox_rejected()
{
  this->reject();
}
