#include <iostream>
#include <string>
#include <vector>

#include <QFont>
#include <QPixmap>

#include <QDebug>
#include <QSqlRecord>

#include "library_table.h"
#include "recipe.h"
#include "settings.h"

LibraryTableModel::LibraryTableModel(QObject *parent)
    :QSqlQueryModel(parent)
{
}


//int LibraryTableModel::rowCount(const QModelIndex & /*parent*/) const
//{
// return listOfRecipes.size();
//}


//int LibraryTableModel::columnCount(const QModelIndex & /*parent*/) const
//{
//  return 2;
//}


QVariant LibraryTableModel::data(const QModelIndex &index, int role) const
{
  int col = index.column();
  int row = index.row();
  switch (role){
  case Qt::DisplayRole:
    if (col == 1){
      QString recipeName = QSqlQueryModel::data(index, role).toString();
      return recipeName;
    }
    //else if (col == 1) {

    //   QModelIndex newIndex = this->index(row,col+1);
    //   int starred = QSqlQueryModel::data(newIndex, role).toInt();
    //   return starred;
    //  }
    break;
  case Qt::FontRole:
    if (col == 1){
    QFont font;
    font.setPixelSize(15);
    font.setBold(true);
    return font;
    }
    break;
  case Qt::DecorationRole:
    if (col == 1){
    QPixmap pix;
    pix.loadFromData(QSqlQueryModel::data(this->index(row, col+1), Qt::DisplayRole).toByteArray());
    if (pix.isNull()){
        pix.load(QString::fromStdString(pineappleImagePath.toStdString()));
      }
    pix = pix.scaled(50,50,Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    return pix;
    }
   };

  return QVariant();
}

int LibraryTableModel::retrieveRecipe(const QModelIndex &index){
  int recipePosition = index.row();
  qDebug() << "Opening recipe "<< QSqlQueryModel::data(this->index(recipePosition, 1)).toString() << " id: " << QSqlQueryModel::data(this->index(recipePosition, 0)).toInt();
  return QSqlQueryModel::data(this->index(recipePosition, 0)).toInt();
}

