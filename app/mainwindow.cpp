#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <string>
#include <iostream>

#include <QFileDialog>
#include <QMessageBox>
#include <QSortFilterProxyModel>

#include "recipeimporter.h"
#include "recipeexporter.h"
#include "recipe.h"
#include "library_table.h"
#include "prefdialog.h"
#include "settings.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->RecipeView->setEditMode(false);

    //Load initial settings
    PrefDialog* prefWindow = new PrefDialog(this);
    prefWindow->loadSettings();
    delete prefWindow;
    dbManager = new RecipeDBManager(this, "/home/tom/Recipes/recipes.db");

    QSettings settings("Recipe Repo", "Recipe Repo");
    if(!settings.value("Library location").toString().isEmpty()){
        recipeLibraryPath = QDir(settings.value("Library location").toString());
      }
      else{
       qDebug() <<"No library location found";
       settings.setValue("Library location", "/home/tom/Recipes/");
       recipeLibraryPath = "/home/tom/Recipes/";
      }
    //    populateDB();


    this->setWindowIcon(QIcon (pineappleImagePath));
    ui->LibrarySidebar->initialise_recipe_menu();
}


MainWindow::~MainWindow(){
    delete ui;
}

//////////////////////////
/// Initialisation
//////////////////////////

std::vector<boost::filesystem::path> findRecipeFiles(const boost::filesystem::path& root, const std::string& ext){
  // Recursively searches through root directory to find .rep files
  // Returns a vector of the paths of these files
  if(boost::filesystem::exists(root) && boost::filesystem::is_directory(root)){
    boost::filesystem::recursive_directory_iterator it(root);
    boost::filesystem::recursive_directory_iterator endit;
    std::vector<boost::filesystem::path> RecipePaths;

    while(it != endit){
      if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ext){
        RecipePaths.push_back(it->path());
      };
      ++it;
    };
    return RecipePaths;
  }
  else{
    throw std::exception();
  };
}

void MainWindow::populateDB()
{
  std::vector<boost::filesystem::path> vect = findRecipeFiles(recipeLibraryPath.path().toStdString(), ".rep");
  if(!vect.empty()){
    for(auto p = vect.begin(); p != vect.end(); ++p){
      dbManager->addRecipe(QString::fromStdString(p->string()));
    };
  };
  dbManager->printRecipes();
}

//////////////////////////
/// Open / Save recipe
//////////////////////////

void MainWindow::openRecipe(Recipe newRecipe){
  ui->RecipeView->setRecipe(newRecipe);
}


void MainWindow::on_Load_Recipe_button_clicked(){

  int recipeIndex = ui->LibrarySidebar->getSelectedIndex();
  openRecipe(dbManager->retrieveRecipe(recipeIndex));
  openRepID = recipeIndex;
}


void MainWindow::on_Library_Menu_tableView_doubleClicked(const QModelIndex &index)
{
  Q_UNUSED(index);
  emit on_Load_Recipe_button_clicked();
}

void MainWindow::saveRecipe(){

  Recipe tempRecipe = ui->RecipeView->getRecipe();
//  if(tempRecipe.Name == openRep.Name){
//    QMessageBox msgBox(this);
//    msgBox.setText("An recipe with this name already exists. Would you like to create a new recipe?");
//    msgBox.setIcon(QMessageBox::Question);
//    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
//    msgBox.setDefaultButton(QMessageBox::Cancel);
//    int response = msgBox.exec();

//    if (response == QMessageBox::Ok){
//      tempRecipe.Name.append(" (copy)");
//    };
//  }

  dbManager->updateRecipe(openRepID, tempRecipe);
  //Reset the display and show the newly saved recipe
  ui->LibrarySidebar->refresh();
  ui->RecipeView->setEditMode(false);
  openRecipe(tempRecipe);
}

//////////////////////////
/// Menubar actions
//////////////////////////

void MainWindow::on_actionEdit_Recipe_triggered(){
  ui->RecipeView->toggleEditMode();
}


void MainWindow::on_actionSave_Recipe_triggered(){
  if (ui->RecipeView->editMode()){
    saveRecipe();
    //opens the new recipe
  };
}


void MainWindow::on_actionDelete_Recipe_triggered(){
  QMessageBox msgBox(this);
  msgBox.setText("Are you sure you want to delete this recipe?");
  msgBox.setIcon(QMessageBox::Question);
  msgBox.setInformativeText(QString("The recipe %1 will be lost").arg(QString(ui->RecipeView->getRecipe().name())));
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Cancel);
  int response = msgBox.exec();

  if (response == QMessageBox::Ok){
    dbManager->deleteRecipe(openRepID);
    ui->LibrarySidebar->refresh();

    int topRecipeID = ui->LibrarySidebar->getFirstRecipeIndex();
    openRecipe(dbManager->retrieveRecipe(topRecipeID));
    openRepID = topRecipeID;
  }
}

void MainWindow::on_actionNew_triggered(){
  Recipe newRecipe;
  MainWindow::openRecipe(newRecipe);
  ui->RecipeView->setEditMode(true);
}


void MainWindow::on_actionAdd_Recipe_triggered(){
  QString filePath = QFileDialog::getOpenFileName(this, tr("Open Recipe File"), recipeLibraryPath.path(), tr("Recipe Files (*.rep)"));
  if (!filePath.isNull()){
    RecipeImporter importer = RecipeImporter(QDir(recipeLibraryPath));
    importer.moveFileToLibrary(filePath);

    Recipe newRecipe = importer.importRecipe(filePath);
    dbManager->addRecipe(newRecipe);

    openRecipe(newRecipe);
    ui->LibrarySidebar->refresh();
  }
}

void MainWindow::on_actionPreferences_triggered(){
  PrefDialog* prefWindow = new PrefDialog(this);
  prefWindow->setAttribute(Qt::WA_DeleteOnClose);
  prefWindow->show();
}
