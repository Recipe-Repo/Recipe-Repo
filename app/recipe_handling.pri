INCLUDEPATH += $$PWD/

SOURCES += $$PWD/recipe.cpp\
    $$PWD/recipedbmanager.cpp \
    $$PWD/recipebuilder.cpp \
    $$PWD/recipeimporter.cpp \
    $$PWD/recipeexporter.cpp


HEADERS  += $$PWD/recipe.h \
    $$PWD/recipedbmanager.h \
    $$PWD/recipebuilder.h \
    $$PWD/recipeimporter.h \
    $$PWD/recipeexporter.h
