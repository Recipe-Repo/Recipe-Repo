#ifndef RECIPEDBMANAGER_H
#define RECIPEDBMANAGER_H

#include <QtSql>

class Recipe;

class RecipeDBManager : public QObject
{
public:
  RecipeDBManager(QObject* parent, const QString& path);
  ~RecipeDBManager();

  bool checkTablesExist();

  bool addRecipe(const Recipe& newRecipe);

  Recipe loadRecipeFile(const int& recipeid);
  QPixmap loadRecipeImage(int recipeID);
  bool updateRecipe(int recipeID, Recipe updatedRecipe);

  void printRecipes();
  Recipe retrieveRecipe(int id);
  Recipe retrieveRecipe(QString recipeName);

  bool deleteRecipe(int id);


private:
  QSqlDatabase m_db;
  void initialiseDB();

  int addRecipeToMainTable(const Recipe& entryRecipe);
  bool addRecipeToSourcesTable(const Recipe& entryRecipe, int recipeID);
  bool addRecipeToIngredientsTable(const Recipe& entryRecipe, int recipeID);
  bool addRecipeToInstructionsTable(const Recipe& entryRecipe, int recipeID);
  bool addRecipeToNotesTable(const Recipe& entryRecipe, int recipeID);
  bool addImagetoDatabase(const Recipe& entryRecipe, int recipeID);

};

#endif // RECIPEDBMANAGER_H
