#include "recipeexporter.h"

#include "recipebuilder.h"
#include "recipe.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>

RecipeExporter::RecipeExporter(QDir path)
{
  libraryPath = path;
}

bool RecipeExporter::exportRecipe(Recipe recipe){

  // Writes recipe to a json file

  QJsonObject j = QJsonObject();
  j["name"] = recipe.name();
  j["ingredients"] =  recipe.ingredients();
  j["instructions"] = recipe.instructions();
  j["picturename"] = recipe.filename() + ".png";
  j["description"] = recipe.description();
  j["active time"] = recipe.activeTime();
  j["total time"] = recipe.totalTime();
  j["source"] = recipe.sourceName();
  j["url"] = recipe.sourceURL();
  j["notes"] = recipe.notes();
  //j["categories"] = QJsonValue(QJsonArray::fromStringList(recipe.Categories));
  j["starred"] = recipe.starred();

  QString fileName = recipe.filename();

  //Make directory for recipe/images
  QDir saveDir = QDir(libraryPath);
  saveDir.mkdir(fileName);
  saveDir.cd(fileName);


  QFile saveFile(saveDir.filePath(fileName + ".rep"));
  saveFile.open(QIODevice::WriteOnly | QIODevice::Text);
  saveFile.write(QJsonDocument(j).toJson());
  saveFile.close();

  if (!recipe.picture().isNull()){
    qDebug() << "Saving image to " << saveDir.filePath(recipe.pictureName() + ".png");
    recipe.picture().save(saveDir.filePath(recipe.pictureName() + ".png"), "PNG");
  }

  return saveFile.exists();
}
