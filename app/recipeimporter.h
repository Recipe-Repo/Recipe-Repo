#ifndef RECIPEIMPORTER_H
#define RECIPEIMPORTER_H
#include <QString>
#include <QDir>

class Recipe;

class RecipeImporter
{
public:

  QDir libraryPath;

  RecipeImporter();
  RecipeImporter(QDir path);
  Recipe importRecipe(QString filePath);
  void moveFileToLibrary(const QString& targetFile);

};

#endif // RECIPEIMPORTER_H
