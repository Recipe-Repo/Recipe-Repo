INCLUDEPATH += $$PWD/

SOURCES += $$PWD/mainwindow.cpp \
    $$PWD/library_table.cpp \
    $$PWD/mainwindow_update.cpp \
    $$PWD/prefdialog.cpp \
    $$PWD/recipeviewer.cpp \
    $$PWD/librarymenu.cpp


HEADERS  += $$PWD/mainwindow.h \
    $$PWD/main.h \
    $$PWD/settings.h \
    $$PWD/library_table.h \
    $$PWD/prefdialog.h \
    $$PWD/recipeviewer.h \
    $$PWD/librarymenu.h

FORMS += $$PWD/mainwindow.ui \
        $$PWD/prefdialog.ui \
        $$PWD/recipeviewer.ui \
    $$PWD/librarymenu.ui


RESOURCES += $$PWD/qdarkstyle/style.qrc
