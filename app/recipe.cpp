#include "recipe.h"
#include "recipebuilder.h"
#include "settings.h"

Recipe::Recipe()
{

}

Recipe::Recipe(QString name){
  Name = name;
}

Recipe::Recipe(const RecipeBuilder &builder){
    this->Name = builder.getName();
    this->Ingredients = builder.getIngredients();
    this->Instructions = builder.getInstructions();
    this->PictureName = builder.getPictureName();
    this->recipePicture = builder.getRecipePicture();
    this->Description = builder.getDescription();
    this->ActiveTime = builder.getActiveTime();
    this->TotalTime = builder.getTotalTime();
    this->SourceName = builder.getSourceName();
    this->SourceURL = builder.getSourceURL();
    this->RecipeNotes = builder.getRecipeNotes();
    this->Categories = builder.getCategories();
    this->StarredRecipe = builder.getStarredRecipe();
}

QString Recipe::name() const{
  return(Name);
}

QString Recipe::ingredients() const{
  return(Ingredients);
}

QString Recipe::instructions() const{
  return(Instructions);
}

QPixmap Recipe::picture() const{
  return(recipePicture);
}

QString Recipe::pictureName() const{
  return(filename());
}

QString Recipe::sourceName() const{
  return(SourceName);
}

QString Recipe::sourceURL() const{
  return(SourceURL);
}

QString Recipe::notes() const{
  return(RecipeNotes);
}

QString Recipe::description() const{
  return(Description);
}

int Recipe::activeTime() const{
  return(ActiveTime);
}

int Recipe::totalTime() const{
  return(TotalTime);
}

bool Recipe::starred() const{
  return(StarredRecipe);
}

bool Recipe::hasSource() const{
 return !(SourceName.isEmpty() && SourceURL.isEmpty());
}

QString Recipe::filename() const{
  /*
   * returns a (not quite) sanitised copy of the recipe name for use as a filename
   * requires handling of /, etc
   */
    std::string nameCopy = Name.toStdString();
    for (unsigned int i = 0; i < nameCopy.length(); i++){
      if (nameCopy[i] == ' '){
          nameCopy[i] = '_';
      }
      nameCopy[i] = tolower(nameCopy[i]);
    }
    return QString::fromStdString(nameCopy);
}
