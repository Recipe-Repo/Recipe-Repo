#include "recipeimporter.h"

#include "recipebuilder.h"
#include "recipeexporter.h"
#include "recipe.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>

#include <QtDebug>

RecipeImporter::RecipeImporter()
{

}

RecipeImporter::RecipeImporter(QDir path)
{
  libraryPath = path;
}

Recipe RecipeImporter::importRecipe(QString filePath){
  // Reads recipe from file and returns Recipe object

  QFile file;
  file.setFileName(filePath);
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  QString val = file.readAll();

  QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
  QJsonObject j = doc.object();

  RecipeBuilder builder;
  builder.name(j["name"].toString("No Name"));
  builder.ingredients(j["ingredients"].toString());
  builder.instructions(j["instructions"].toString());
  builder.pictureName(j["picturename"].toString());
  builder.description(j["description"].toString());
  builder.activeTime(j["active time"].toInt(0));
  builder.totalTime(j["total time"].toInt(0));
  builder.sourceName(j["source"].toString());
  builder.sourceURL(j["url"].toString());
  builder.recipeNotes(j["notes"].toString());
  //QList<QVariant> tempCategories = j["categories"].toArray().toVariantList();
  //std::transform(tempCategories.begin(), tempCategories.end(), std::back_inserter(rep.Categories), [](const QVariant& category) { return category.toString();});
  builder.starredRecipe(j["starred"].toBool(false));

  //Get directory of recipe file
  QDir recipeLocation = filePath;
  recipeLocation.cdUp();

  //Append relative path of picture file and load file.
  QString picturePath = recipeLocation.path() + QDir::separator() + builder.getPictureName();
  builder.recipePicture(QPixmap(picturePath));

  return builder.build();
}

void RecipeImporter::moveFileToLibrary(const QString& targetPath){
  /*
   * Needs handling for issues such as invalid target file, etc
   */

  RecipeExporter(libraryPath).exportRecipe(importRecipe(targetPath));
}
