#ifndef LIBRARYTABLEMODEL_H
#define LIBRARYTABLEMODEL_H

#include <QAbstractTableModel>
#include <QSqlQueryModel>
#include <string>

class LibraryTableModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    LibraryTableModel(QObject *parent);

    /*
    * Returns the number of rows in the table
    * @param QModelIndex parent: unused parameter
    * @return int: number of columns in the model
    */
    //int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /*
    * Returns the number of columns in the table
    * @param QModelIndex parent: unused parameter
    * @return int: number of rows in the model
    */
    //int columnCount(const QModelIndex &parent = QModelIndex()) const;

    /*
    * Returns the data contained in the table at a given index
    * @param QModelIndex index: index of the cell which is being read with (0,0) being the top left cell
    * @param int role: Which characteristic of the cell to return. eg. text, font, background colour, etc.
    * @return QVariant: The specified characteristic of the chosen cell.
    */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    /*
    * Adds a number of rows to the table
    * @param int position: position in the table to add the new rows
    * @param int rows: The number of new rows to be added to the table
    * @param QModelIndex parent: unused parameter
    * @return bool: bool of wheter the rows have been successfully added.
    */
   // bool insertRows(int position, int rows, const QModelIndex &index);

    /*
    * Sets the data contained in a cell of the table at a given index
    * @param QModelIndex index: index of the cell which is being written to with (0,0) being the top left cell
    * @param QVariant value: The new data to be writen to the cell
    * @param int role: Which characteristic of the cell to overwrite. eg. text, font, background colour, etc.
    * @return bool: bool of whether the data has been successfully written
    */
//    bool setData(const QModelIndex &index, const QVariant &value, int role);

    /*
    * Helper function to append a row to the table and fill it with the filename of a new recipe
    * @param std::string recipeFilename: filename of the file which is to be added to the table
    * @return bool: bool of whether the recipe has been successfully added to the table
    */
    //bool addRecipe(std::string recipeFilename, const QModelIndex &index);

    /*
     * Returns the Recipe object corresponding to a given index of the table
     * @param QModelIndex index: Index of the given recipe in the model
     * @return Recipe: Recipe object stored at given index
     */

    int retrieveRecipe(const QModelIndex &index);


    /*
    * Removes all recipes from the table.
    * @return void
    */
    //void clearList();

private:
    //QList<Recipe> listOfRecipes;
};

#endif // LIBRARYTABLEMODEL_H
