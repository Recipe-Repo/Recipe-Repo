#ifndef RECIPEVIEWER_H
#define RECIPEVIEWER_H

#include <QWidget>
#include <QGraphicsScene>

class Recipe;
class QPixmap;


namespace Ui {
class RecipeViewer;
}

class RecipeViewer : public QWidget
{
  Q_OBJECT

public:
  explicit RecipeViewer(QWidget *parent = nullptr);
  ~RecipeViewer();

  void setRecipe(Recipe recipe);
  Recipe getRecipe();

  bool editMode();
  void setEditMode(bool val);
  void toggleEditMode();


private slots:

  /*
   * Opens the source url of the open recipe in the user's default browser
   */
  void on_Source_button_clicked();

  void on_Recipe_Image_button_clicked();

private:
  Ui::RecipeViewer *ui;

  /*
   * Self explanatory: updates named widget with given parameter
   */
  void update_Recipe_Name_label(QString newName);
  void update_Starred_Recipe_checkbox(bool newStarred);
  void update_Recipe_Image_label(QPixmap newImage);
  void update_Recipe_Image_button(QString newImagePath);
  void update_Source_button(QString newSourceName);
  void update_Active_Time_timeEdit(int newActiveTime);
  void update_Total_Time_timeEdit(int newTotalTime);
  void update_Description_textEdit(QString newDescription);
  void update_Notes_textEdit(QString newNotes);
  void update_Ingredients_textEdit(std::string newIngredients);
  void update_Instructions_textEdit(QString newInstructions);

  bool editFlag = false;
  QGraphicsScene* recipeImage = nullptr;
  QPixmap recipePix;

};

#endif // RECIPEVIEWER_H
