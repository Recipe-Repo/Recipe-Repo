#ifndef PREFDIALOG_H
#define PREFDIALOG_H

#include <QDialog>

namespace Ui {
class PrefDialog;
}

class PrefDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PrefDialog(QWidget *parent = 0);
    ~PrefDialog();
    bool loadSettings();

private slots:
    void on_buttonBox_accepted();

    void on_Library_Path_pushButton_clicked();

    bool saveSettings();

    void on_buttonBox_rejected();

private:
    Ui::PrefDialog *ui;
};

#endif // PREFDIALOG_H
