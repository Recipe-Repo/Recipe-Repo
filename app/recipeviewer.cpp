#include "recipeviewer.h"
#include "ui_recipeviewer.h"

#include "recipebuilder.h"
#include "recipe.h"
#include "settings.h"

#include <QFileDialog>

#include <QDebug>

RecipeViewer::RecipeViewer(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::RecipeViewer)
{
  ui->setupUi(this);


  recipeImage = new QGraphicsScene();
  ui->graphicsView->setScene(recipeImage);
  recipeImage->addPixmap(QPixmap(pineappleImagePath));
}

RecipeViewer::~RecipeViewer()
{
  delete ui;
}

void RecipeViewer::setRecipe(Recipe recipe){

  update_Recipe_Name_label(recipe.name());
  update_Starred_Recipe_checkbox(recipe.starred());
  update_Recipe_Image_label(recipe.picture());
  update_Recipe_Image_button("Select new image");
  update_Source_button(recipe.sourceName());
  update_Active_Time_timeEdit(recipe.activeTime());
  update_Total_Time_timeEdit(recipe.totalTime());
  update_Description_textEdit(recipe.description());
  update_Notes_textEdit(recipe.notes());
  update_Ingredients_textEdit(recipe.ingredients().toStdString());
  update_Instructions_textEdit(recipe.instructions());
  ui->Recipe_Name_lineEdit->setText(recipe.name());
  ui->Source_Name_lineEdit->setText(recipe.sourceName());
  ui->Source_URL_lineEdit->setText(recipe.sourceURL());
  ui->tabWidget->setCurrentIndex(0);
}

Recipe RecipeViewer::getRecipe(){
  RecipeBuilder builder;
  builder.name(ui->Recipe_Name_lineEdit->text());
  builder.ingredients(ui->Ingredients_textEdit->toPlainText());
  builder.instructions(ui->Instructions_textEdit->toPlainText());
  builder.recipePicture(recipePix);
  builder.description(ui->Description_textEdit->toPlainText());
  builder.activeTime(60 * ui->Active_Time_timeEdit->time().hour() + ui->Active_Time_timeEdit->time().minute());
  builder.totalTime(60 * ui->Total_Time_timeEdit->time().hour() + ui->Total_Time_timeEdit->time().minute());
  builder.sourceName(ui->Source_Name_lineEdit->text());
  builder.sourceURL(ui->Source_URL_lineEdit->text());
  builder.recipeNotes(ui->Notes_textEdit->toPlainText());
  //QList<QVariant> tempCategories = j["categories"].toArray().toVariantList();
  //std::transform(tempCategories.begin(), tempCategories.end(), std::back_inserter(rep.Categories), [](const QVariant& category) { return category.toString();});
  builder.starredRecipe(ui->Starred_Recipe_checkbox->isChecked());

  return(builder.build());
}

bool RecipeViewer::editMode(){
  return editFlag;
}

void RecipeViewer::setEditMode(bool val){

  editFlag = val;
  qDebug() << "Edit mode set to " << editFlag;

  // populate textboxes with the relevant data
  ui->Recipe_Name_label->setHidden(editFlag);
  ui->Recipe_Name_lineEdit->setHidden(!editFlag);
  ui->Recipe_Image_button->setHidden(!editFlag);
  ui->Source_button->setHidden(editFlag);
  ui->Source_Name_lineEdit->setHidden(!editFlag);
  ui->Source_URL_lineEdit->setHidden(!editFlag);
  ui->Active_Time_timeEdit->setReadOnly(!editFlag);
  ui->Total_Time_timeEdit->setReadOnly(!editFlag);
  ui->Description_textEdit->setReadOnly(!editFlag);
  ui->Notes_textEdit->setReadOnly(!editFlag);
  ui->Ingredients_textEdit->setReadOnly(!editFlag);
  ui->Instructions_textEdit->setReadOnly(!editFlag);
}

void RecipeViewer::toggleEditMode(){
  setEditMode(!editFlag);
}

void RecipeViewer::on_Recipe_Image_button_clicked(){
  QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), /*recipeLibraryPath.path(),*/ tr("Image Files (*.png *.jpg);;All Files (*.*)"));
  QPixmap newImage = QPixmap(fileName);
  update_Recipe_Image_label(newImage);
  update_Recipe_Image_button(fileName);
}

void RecipeViewer::on_Source_button_clicked()
{
  QString name = ui->Source_Name_lineEdit->text();
  QString url = ui->Source_URL_lineEdit->text();
  if (!url.isEmpty()){
    QString systemCommand = "xdg-open " + url;
    system(systemCommand.toStdString().c_str());
    //return true;
  }
  else if (!name.isEmpty()){
    //return true;
  }
  else {
    ui->Source_button->setText("No source has been set for this recipe. Please go into edit mode and enter one.");
    //return false;
  }
}
