#ifndef RECIPEBUILDER_H
#define RECIPEBUILDER_H
#include <QString>
#include <QPixmap>

class Recipe;


class RecipeBuilder
{
private:
    QString Name;
    QString Ingredients;
    QString Instructions;
    QString PictureName;
    QPixmap RecipePicture;
    QString Description;
    int ActiveTime = 0;
    int TotalTime = 0;
    QString SourceName;
    QString SourceURL;
    QString RecipeNotes;
    QList<QString> Categories;
    bool StarredRecipe = false;


public:
    RecipeBuilder();

    void name(QString name);
    void ingredients(QString ingredients);
    void instructions(QString instructions);
    void pictureName(QString name);
    void recipePicture(QPixmap name);
    void description(QString description);
    void activeTime(int activeTime);
    void totalTime(int totalTime);
    void sourceName(QString name);
    void sourceURL(QString url);
    void recipeNotes(QString notes);
    void categories(QList<QString> categories);
    void starredRecipe(bool starred);

    QString getName() const;
    QString getIngredients() const;
    QString getInstructions() const;
    QString getPictureName() const;
    QPixmap getRecipePicture() const;
    QString getDescription() const;
    int getActiveTime() const;
    int getTotalTime() const;
    QString getSourceName() const;
    QString getSourceURL() const;
    QString getRecipeNotes() const;
    QList<QString> getCategories() const;
    bool getStarredRecipe() const;

    Recipe build();
};

#endif // RECIPEBUILDER_H
