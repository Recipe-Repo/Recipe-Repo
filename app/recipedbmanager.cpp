#include "recipedbmanager.h"
#include <iostream>

#include "recipeimporter.h"
#include "recipebuilder.h"
#include "recipe.h"


RecipeDBManager::RecipeDBManager(QObject* parent, const QString& path) : QObject(parent)
{
  m_db = QSqlDatabase::addDatabase("QSQLITE");
  m_db.setDatabaseName(path);

  if (m_db.open())
  {
    if(!checkTablesExist()){
      qDebug() << "Can't find tables in database... Initialising...";
      initialiseDB();
    }
    else{
      qDebug() << "Database: connection ok";
    }
    QSqlQuery("PRAGMA foreign_keys = 1");

  }
  else
  {
    qDebug() << "Error: connection with database fail";
  }
}

RecipeDBManager::~RecipeDBManager(){
  m_db.close();
  if(!m_db.isOpen()){
    qDebug() << "Database closed successfully";
  }
  else{
    qDebug() << "Database still open";
  }
}

bool RecipeDBManager::checkTablesExist(){
  //Check if recipes table exists, we assume that other tables exist.
  QSqlQuery query("SELECT name FROM sqlite_master WHERE type='table' AND name='recipes'");
  bool validDatabase = false;
  if(query.exec()){
    if(query.first()){
      validDatabase = true;
    }
  }
  return validDatabase;
}

void RecipeDBManager::initialiseDB(){
  QSqlQuery("CREATE TABLE recipes(recipeid integer primary key, name text, starred integer, activetime integer, totaltime integer)").exec();
  QSqlQuery("CREATE TABLE sources(recipeid integer primary key, sourcename text, sourceurl text, FOREIGN KEY (recipeid) REFERENCES recipes(recipeid) ON DELETE CASCADE)").exec();
  QSqlQuery("CREATE TABLE recipenotes(recipeid integer primary key, description text, notes text, FOREIGN KEY (recipeid) REFERENCES recipes(recipeid) ON DELETE CASCADE)").exec();
  QSqlQuery("CREATE TABLE instructions(recipeid integer primary key, instructions text, FOREIGN KEY (recipeid) REFERENCES recipes(recipeid) ON DELETE CASCADE)").exec();
  QSqlQuery("CREATE TABLE images(imageid integer primary key, recipeid integer, imagedata blob, FOREIGN KEY (recipeid) REFERENCES recipes(recipeid) ON DELETE CASCADE)").exec();
  QSqlQuery("CREATE TABLE ingredients(recipeid primary key, ingredient text, FOREIGN KEY (recipeid) REFERENCES recipes(recipeid) ON DELETE CASCADE)").exec();
}

void RecipeDBManager::printRecipes(){
  QSqlQuery query("SELECT name FROM recipes");
  int idName = query.record().indexOf("name");
  while (query.next())
  {
     QString name = query.value(idName).toString();
     qDebug() << name;
  }
}

/////////////////////////////////////////////
// Importing recipe files
/////////////////////////////////////////////

bool RecipeDBManager::addRecipe(const Recipe& newRecipe)
{
  int newRecipeID = addRecipeToMainTable(newRecipe);

//  if(newRecipe.hasSource()){
    addRecipeToSourcesTable(newRecipe, newRecipeID);
//  }

//  if(!newRecipe.Ingredients.isEmpty()){
    addRecipeToIngredientsTable(newRecipe, newRecipeID);
//  }

//  if(!newRecipe.Instructions.isEmpty()){
    addRecipeToInstructionsTable(newRecipe, newRecipeID);
//  }
//  if(!newRecipe.RecipeNotes.isEmpty() || !newRecipe.Description.isEmpty()){
    addRecipeToNotesTable(newRecipe, newRecipeID);
//  }

//  if(!newRecipe.recipePicture.isNull()){
    addImagetoDatabase(newRecipe, newRecipeID);
//  }
    bool success = true;
    return success;
}

int RecipeDBManager::addRecipeToMainTable(const Recipe& entryRecipe){
  QSqlQuery query;
  query.prepare("INSERT INTO recipes (name, starred, activetime, totaltime) VALUES (:name, :star, :activetime, :totaltime)");
  query.bindValue(":name", entryRecipe.name());
  query.bindValue(":star", entryRecipe.starred());
  query.bindValue(":activetime", entryRecipe.activeTime());
  query.bindValue(":totaltime", entryRecipe.totalTime());
  query.exec();

  int recipeid = 0;
  QSqlQuery recipeIdQuery = QSqlQuery("SELECT last_insert_rowid()");
  if(recipeIdQuery.first()){
   recipeid = recipeIdQuery.value(0).toInt();
  }
  return recipeid;

}

bool RecipeDBManager::addRecipeToSourcesTable(const Recipe& entryRecipe, int recipeID){
  QSqlQuery query;
  bool success = false;

  query.prepare("INSERT INTO sources(recipeid, sourcename, sourceurl) VALUES (:recipeid, :sourcename, :sourceurl)");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":sourcename", entryRecipe.sourceName());
  query.bindValue(":sourceurl", entryRecipe.sourceURL());
  if(query.exec())
  {
    success = true;
  }
  else
  {
    qDebug() << "addRecipeToSourcesTable error:  " << query.lastError();
  }
  return success;
}

bool RecipeDBManager::addRecipeToIngredientsTable(const Recipe& entryRecipe,int recipeID){
  QSqlQuery query;
  bool success = false;

  //for (auto Ingredient = entryRecipe.Ingredients.begin(); Ingredient != entryRecipe.Ingredients.end(); ++Ingredient){
    query.prepare("INSERT INTO ingredients (recipeid, ingredient) VALUES (:recipeid, :ingredient)");
    query.bindValue(":recipeid", recipeID);
    query.bindValue(":ingredient",entryRecipe.ingredients());
    if(query.exec())
    {
      success = true;
    }
    else
    {
      qDebug() << "addRecipeToIngredientsTables error:  " << query.lastError();
    }
  //}

  return success;
}

bool RecipeDBManager::addRecipeToInstructionsTable(const Recipe& entryRecipe, int recipeID){
  QSqlQuery query;
  bool success = false;

  query.prepare("INSERT INTO instructions(recipeid, instructions) VALUES (:recipeid, :instructions)");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":instructions", entryRecipe.instructions());
  if(query.exec())
  {
    success = true;
  }
  else
  {
    qDebug() << "addRecipeToInstructionsTable error:  " << query.lastError();
  }
  return success;
}

bool RecipeDBManager::addRecipeToNotesTable(const Recipe& entryRecipe, int recipeID){
  QSqlQuery query;
  query.prepare("INSERT INTO recipenotes (recipeid, description, notes) VALUES (:recipeid, :description, :notes)");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":description", entryRecipe.description());
  query.bindValue(":notes", entryRecipe.notes());
  bool success = false;
  if(query.exec())
  {
    success = true;
  }
  else
  {
    qDebug() << "addRecipe error:  " << query.lastError();
  }

  return success;
}

bool RecipeDBManager::addImagetoDatabase(const Recipe& entryRecipe, int recipeID){
  bool success = false;
  QSqlQuery query;
  query.prepare("INSERT INTO images (recipeid, imagedata) VALUES (:recipeid, :imagefile)");
  query.bindValue(":recipeid", recipeID);

  if (!entryRecipe.picture().isNull()){
      QByteArray inByteArray;
      QBuffer buffer(&inByteArray);
      buffer.open(QIODevice::WriteOnly);
      entryRecipe.picture().save(&buffer, "PNG");
      query.bindValue(":imagefile", inByteArray);
      if(query.exec()){
        success = true;
      }
  }
  return success;
}

bool RecipeDBManager::updateRecipe(int recipeID, Recipe updatedRecipe){
  QSqlQuery query;
  query.prepare("UPDATE recipes SET name = :name, starred = :starred, activetime = :activetime, totaltime = :totaltime WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":name", updatedRecipe.name());
  query.bindValue(":starred", updatedRecipe.starred());
  query.bindValue(":activetime", updatedRecipe.activeTime());
  query.bindValue(":totaltime", updatedRecipe.totalTime());
  query.exec();

  query.prepare("UPDATE ingredients SET ingredient = :ingredient WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":ingredient", updatedRecipe.ingredients());
  query.exec();

  query.prepare("UPDATE instructions SET instructions = :instructions WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":instructions", updatedRecipe.instructions());
  query.exec();

  query.prepare("UPDATE sources SET sourcename = :sourcename, sourceurl = :sourceurl WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":sourcename", updatedRecipe.sourceName());
  query.bindValue(":sourceurl", updatedRecipe.sourceURL());
  query.exec();

  query.prepare("UPDATE recipenotes SET description = :description, notes = :notes WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);
  query.bindValue(":description", updatedRecipe.description());
  query.bindValue(":notes", updatedRecipe.notes());
  query.exec();

  if (!updatedRecipe.picture().isNull()){
      QByteArray inByteArray;
      QBuffer buffer(&inByteArray);
      buffer.open(QIODevice::WriteOnly);
      updatedRecipe.picture().save(&buffer, "PNG");
      query.prepare("UPDATE images SET imagedata = :imagedata WHERE recipeid = :recipeid");
      query.bindValue(":recipeid", recipeID);
      query.bindValue(":imagedata", inByteArray);
      query.exec();
  }

  bool success = true;
  return success;
}

/////////////////////////////////////////////
// Data Retreival
/////////////////////////////////////////////

Recipe RecipeDBManager::retrieveRecipe(int id){
  QSqlQuery query;
  query.prepare("SELECT name, ingredient, instructions, sourcename, sourceurl, activetime, totaltime, starred, description, notes FROM recipes INNER JOIN recipenotes ON recipes.recipeid == recipenotes.recipeid INNER JOIN ingredients ON recipes.recipeid == ingredients.recipeid INNER JOIN instructions ON recipes.recipeid == instructions.recipeid INNER JOIN sources ON recipes.recipeid == sources.recipeid WHERE recipes.recipeid = :recipeid");
  query.bindValue(":recipeid", id);
  Recipe entryRecipe;
  RecipeBuilder builder;
  if (query.exec()){
    query.next();

    builder.name(query.value(query.record().indexOf("name")).toString());
    builder.ingredients(query.value(query.record().indexOf("ingredient")).toString());
    builder.instructions(query.value(query.record().indexOf("instructions")).toString());
    builder.recipePicture(loadRecipeImage(id));
    builder.description(query.value(query.record().indexOf("description")).toString());
    builder.activeTime(query.value(query.record().indexOf("activetime")).toInt());
    builder.totalTime(query.value(query.record().indexOf("totaltime")).toInt());
    builder.sourceName(query.value(query.record().indexOf("sourcename")).toString());
    builder.sourceURL(query.value(query.record().indexOf("sourceurl")).toString());
    builder.recipeNotes(query.value(query.record().indexOf("notes")).toString());
    //builder.Categories = tempentryRecipe.Categories;
    builder.starredRecipe(query.value(query.record().indexOf("starred")).toInt());
    entryRecipe = builder.build();
  }
  else{
    qDebug() << "retrieveRecipe: " << query.lastError();
  }
  return entryRecipe;
}

Recipe RecipeDBManager::retrieveRecipe(QString recipeName){
    QSqlQuery query;
    query.prepare("SELECT recipeid FROM recipes WHERE recipes.name = :name");
    query.bindValue(":name", recipeName);
    if (query.exec()){
      query.next();
      if(query.size() > 1){
        qDebug() << "Multiple recipes returned (Found " << query.size() << " records)";
      }
      int recipeID = query.value(query.record().indexOf("recipeid")).toInt();
      return retrieveRecipe(recipeID);
    }
    else{
      qDebug() << "retrieveRecipe: " << query.lastError();
    }
    return Recipe();
}

QPixmap RecipeDBManager::loadRecipeImage(int recipeID){
  QSqlQuery query;
  query.prepare("SELECT imagedata FROM images where recipeid = :recipeid");
  query.bindValue(":recipeid", recipeID);

  QPixmap recipePixmap;
  if(query.exec()){
    query.first();
    QByteArray outByteArray = query.value(query.record().indexOf("imagedata")).toByteArray();
    recipePixmap.loadFromData(outByteArray);
  }
  else{
    qDebug() << query.lastError();
  }
  return recipePixmap;
}

bool RecipeDBManager::deleteRecipe(int id){
  QSqlQuery query;
  query.prepare("DELETE FROM recipes WHERE recipeid = :recipeid");
  query.bindValue(":recipeid", id);
  bool deleteSuccess = false;
  if (query.exec()){
    deleteSuccess = true;
  }
  else{
    qDebug() << query.lastError();
  }
  return deleteSuccess;

}
