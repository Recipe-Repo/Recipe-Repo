#include "recipebuilder.h"
#include "recipe.h"

RecipeBuilder::RecipeBuilder()
{

}

Recipe RecipeBuilder::build(){
    return Recipe(*this);
}

void RecipeBuilder::name(QString name){
    this->Name = name;
}

QString RecipeBuilder::getName() const{
    return Name;
}

void RecipeBuilder::ingredients(QString ingredients){
    this->Ingredients = ingredients;
}

QString RecipeBuilder::getIngredients() const{
    return Ingredients;
}

void RecipeBuilder::instructions(QString instructions){
    this->Instructions = instructions;
}

QString RecipeBuilder::getInstructions() const{
    return Instructions;
}

void RecipeBuilder::pictureName(QString name){
    this->PictureName = name;
}

QString RecipeBuilder::getPictureName() const{
    return PictureName;
}

void RecipeBuilder::recipePicture(QPixmap name){
    this->RecipePicture = name;
}

QPixmap RecipeBuilder::getRecipePicture() const{
    return RecipePicture;
}

void RecipeBuilder::description(QString description){
    this->Description = description;
}

QString RecipeBuilder::getDescription() const{
    return Description;
}

void RecipeBuilder::activeTime(int activeTime){
    if (activeTime >= 0){
        this->ActiveTime = activeTime;
    } else{
        throw std::domain_error("received negative time");
    }
}

int RecipeBuilder::getActiveTime() const{
    return ActiveTime;
}

void RecipeBuilder::totalTime(int totalTime){
    if (totalTime >= 0){
        this->TotalTime = totalTime;
    } else{
        throw std::domain_error("received negative time");
    }
}

int RecipeBuilder::getTotalTime() const{
    return TotalTime;
}

void RecipeBuilder::sourceName(QString name){
    this->SourceName = name;
}

QString RecipeBuilder::getSourceName() const{
    return SourceName;
}

void RecipeBuilder::sourceURL(QString url){
    this->SourceURL = url;
}

QString RecipeBuilder::getSourceURL() const{
    return SourceURL;
}

void RecipeBuilder::recipeNotes(QString notes){
    this->RecipeNotes = notes;
}

QString RecipeBuilder::getRecipeNotes() const{
    return RecipeNotes;
}

void RecipeBuilder::categories(QList<QString> categories){
    this->Categories = categories;
}

QList<QString> RecipeBuilder::getCategories() const{
    return Categories;
}

void RecipeBuilder::starredRecipe(bool starred){
    this->StarredRecipe = starred;
}

bool RecipeBuilder::getStarredRecipe() const{
    return StarredRecipe;
}
