#ifndef RECIPEEXPORTER_H
#define RECIPEEXPORTER_H
#include <QDir>

class Recipe;

class RecipeExporter
{
public:

  QDir libraryPath;

  RecipeExporter(QDir path);
  bool exportRecipe(Recipe recipe);
};

#endif // RECIPEEXPORTER_H
