#include "librarymenu.h"
#include "ui_librarymenu.h"

LibraryMenu::LibraryMenu(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::LibraryMenu)
{
  ui->setupUi(this);
}

LibraryMenu::~LibraryMenu()
{
  delete ui;
}

void LibraryMenu::initialise_recipe_menu(){
  libraryModel = new LibraryTableModel(this);
  libraryModel->setQuery("SELECT recipes.recipeid, name, imagedata FROM recipes LEFT JOIN images ON recipes.recipeid == images.recipeid ORDER BY name");
  ui->Library_Menu_tableView->setModel(libraryModel);
  //queryModel->setQuery("SELECT * FROM recipes");
  ui->Library_Menu_tableView->horizontalHeader()->setStretchLastSection(true);
  ui->Library_Menu_tableView->hideColumn(0);
  ui->Library_Menu_tableView->hideColumn(2);
  ui->Library_Menu_tableView->horizontalHeader()->hide();
  ui->Library_Menu_tableView->verticalHeader()->hide();
  ui->Library_Menu_tableView->resizeColumnToContents(0);
  ui->Library_Menu_tableView->resizeRowsToContents();
  ui->Library_Menu_tableView->setFocusPolicy(Qt::ClickFocus);
  ui->Load_Recipe_button->setFocusPolicy(Qt::ClickFocus);
}

void LibraryMenu::refresh(){
  libraryModel->setQuery("SELECT recipes.recipeid, name, imagedata FROM recipes LEFT JOIN images ON recipes.recipeid == images.recipeid ORDER BY name");
  ui->Library_Menu_tableView->resizeColumnToContents(0);
  ui->Library_Menu_tableView->resizeRowsToContents();
}

int LibraryMenu::getSelectedIndex(){
  int selectedRow = ui->Library_Menu_tableView->currentIndex().row();
  QModelIndex index = ui->Library_Menu_tableView->model()->index(selectedRow,0);
  return(getRecipeIndex(index));
}

int LibraryMenu::getFirstRecipeIndex(){
  QModelIndex index = ui->Library_Menu_tableView->model()->index(0,0);
  return(getRecipeIndex(index));
}


int LibraryMenu::getRecipeIndex(const QModelIndex &index){
  int clickedRecipeID = libraryModel->retrieveRecipe(index);
  return(clickedRecipeID);
}
