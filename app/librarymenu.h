#ifndef LIBRARYMENU_H
#define LIBRARYMENU_H

#include <QWidget>

#include "library_table.h"


namespace Ui {
class LibraryMenu;
}

class LibraryMenu : public QWidget
{
  Q_OBJECT

public:
  explicit LibraryMenu(QWidget *parent = nullptr);
  ~LibraryMenu();

  void refresh();

  /*
   * Initialises the recipe menu in the bar on the left of the window.
   * Ensures that names aren't cut off, etc.
   */
  void initialise_recipe_menu();

  int getSelectedIndex();

  int getRecipeIndex(const QModelIndex &index);

  int getFirstRecipeIndex();



private:
  Ui::LibraryMenu *ui;

  LibraryTableModel* libraryModel;
};

#endif // LIBRARYMENU_H
