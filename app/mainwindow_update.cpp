#include "recipeviewer.h"
#include "ui_recipeviewer.h"

#include <string>
#include <boost/filesystem.hpp>
#include "settings.h"

void RecipeViewer::update_Recipe_Name_label(QString newName){
  ui->Recipe_Name_label->setText(newName);
}


void RecipeViewer::update_Starred_Recipe_checkbox(bool newStarred){
  ui->Starred_Recipe_checkbox->setChecked(newStarred);
}


void RecipeViewer::update_Source_button(QString newSourceName){
  ui->Source_button->setText(newSourceName);
}


void RecipeViewer::update_Active_Time_timeEdit(int newActiveTime){
  int hours = newActiveTime / 60;
  int mins = newActiveTime % 60;
  QTime time(hours, mins);
  ui->Active_Time_timeEdit->setTime(time);
}


void RecipeViewer::update_Total_Time_timeEdit(int newTotalTime){
  int hours = newTotalTime / 60;
  int mins = newTotalTime % 60;
  QTime time(hours, mins);
  ui->Total_Time_timeEdit->setTime(time);
}


void RecipeViewer::update_Description_textEdit(QString newDescription){
  ui->Description_textEdit->setText(newDescription);
}


void RecipeViewer::update_Notes_textEdit(QString newNotes){
  ui->Notes_textEdit->setText(newNotes);
}


void RecipeViewer::update_Ingredients_textEdit(std::string newIngredients){
  ui->Ingredients_textEdit->setText(newIngredients.c_str());
}


void RecipeViewer::update_Instructions_textEdit(QString newInstructions){
  ui->Instructions_textEdit->setText(newInstructions);
}

void RecipeViewer::update_Recipe_Image_label(QPixmap newImage){
  if(recipeImage != nullptr){
    delete recipeImage;
    recipeImage = new QGraphicsScene();
    ui->graphicsView->setScene(recipeImage);
  };

  recipeImage->addPixmap(newImage);
  ui->graphicsView->fitInView(recipeImage->sceneRect(), Qt::KeepAspectRatio);

  recipePix = newImage;
}

void RecipeViewer::update_Recipe_Image_button(QString newImagePath){
  ui->Recipe_Image_button->setText(newImagePath);
}
