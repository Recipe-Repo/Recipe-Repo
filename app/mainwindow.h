#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <string>
#include <boost/filesystem.hpp>
#include "recipe.h"
#include <recipedbmanager.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();


  void populateDB();

  /*
   * Reads user-inputted data from gui and constructs a Recipe
   * The Recipe is then saved to the filesystem
   */
  void saveRecipe();

  void openRecipe(Recipe newRecipe);

signals:

  void refreshLibrary();

  void updateSettings();

private slots:

  /*
   * Opens the recipe selected in the sidebar and displays it
   */
  void on_Load_Recipe_button_clicked();

  /*
   * See slot on_Load_Recipe_button_clicked()
   */

  void on_Library_Menu_tableView_doubleClicked(const QModelIndex &index);

  /*
   * Creates a new empty recipe and enables editMode.
   */
  void on_actionNew_triggered();

  /*
   * Toggles flag editMode to enable or disable editing of recipes
   */
  void on_actionEdit_Recipe_triggered();

  /*
   * Saves the current recipe to the filesystem
   */

  void on_actionSave_Recipe_triggered();

  /*
   * Deletes the current recipe from the filesystem
   */

  void on_actionDelete_Recipe_triggered();

  void on_actionAdd_Recipe_triggered();

  void on_actionPreferences_triggered();


  //void on_actionExport_Recipe_triggered();

private:
  Ui::MainWindow *ui;
  int openRepID;
  QDir recipeLibraryPath;
  RecipeDBManager* dbManager = nullptr;
};

#endif // MAINWINDOW_H
