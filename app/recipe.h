#ifndef RECIPE_H
#define RECIPE_H

#include <string>
#include <QString>
#include <QList>
#include <QPixmap>

class RecipeBuilder;

class Recipe
{
private:

  QString Name;
  QString Ingredients;
  QString Instructions;
  QString PictureName;
  QPixmap recipePicture;
  QString Description;
  int ActiveTime = 0;
  int TotalTime = 0;
  QString SourceName;
  QString SourceURL;
  QString RecipeNotes;
  QList<QString> Categories;
  bool StarredRecipe = false;

public:
  Recipe();
  Recipe(QString name);
  Recipe(const RecipeBuilder &builder);

  QString name() const;
  QString ingredients() const;
  QString instructions() const;
  QPixmap picture() const;
  QString pictureName() const;
  QString sourceName() const;
  QString sourceURL() const;
  QString notes() const;
  QString description() const;
  int activeTime() const;
  int totalTime() const;
  bool starred() const;

  QString filename() const;
  void deleteRecipeFile();

  bool hasSource() const;

  bool operator < (const Recipe& str) const{
    return (Name < str.Name);
  }
};

#endif // RECIPE_H

